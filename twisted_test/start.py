"""
На twisted(предпочтительно) или asyncio написать простейший скрипт реализующий клиент и сервер TCP.
Проверку будем осуществлять следующим образом:

    telnet 127.0.0.1 10000 ->
        [сервер порт 10000] скрипт1 [клиент]->
        [сервер порт 10001] скрипт2 [сервер порт 10002]
    <- telnet 127.0.0.1 10002

В итоге при наборе текста в одном telnet клиенте этот текст должен отображаться в другом.
"""

import run_module_1
import run_module_2

from client import BaseClientFactory
from base_server import ServerFactory

from twisted.internet import reactor


reactor.listenTCP(10000, ServerFactory(
        run_module=run_module_1
    )
)
reactor.listenTCP(10001, ServerFactory(
        run_module=run_module_2,
        http_requests=[
            {'host': 'localhost', 'port': 10002}
        ]
    ),
)
reactor.listenTCP(10002, ServerFactory(
        http_requests=[
            {'host': 'localhost', 'port': 10001}
        ]
    )
)

clients = []

reactor.connectTCP('localhost', 10000, BaseClientFactory(clients))
reactor.connectTCP('localhost', 10001, BaseClientFactory(clients))

reactor.run()
