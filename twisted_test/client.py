from twisted.internet.protocol import Protocol, ClientFactory


class Client(Protocol):
    def __init__(self, clients):
        self.clients = clients

    def connectionMade(self):
        self.transport.write(b'type:proxy_client\r\n')

    def dataReceived(self, data):
        # Отправляем сообщение всем серверам кроме себя
        for client in self.clients:
            if client != self:
                client.transport.write(data)


class BaseClientFactory(ClientFactory):
    def __init__(self, clients):
        self.clients = clients

    def buildProtocol(self, addr):
        client = Client(clients=self.clients)
        self.clients.append(client)
        return client
