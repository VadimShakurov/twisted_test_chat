import uuid

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet.protocol import Factory, Protocol, connectionDone
from twisted.web.client import Agent
from twisted.web.iweb import IBodyProducer

from zope.interface import implementer


@implementer(IBodyProducer)
class StringProducer(object):
    def __init__(self, body):
        self.body = body
        self.length = len(body)

    def startProducing(self, consumer):
        consumer.write(self.body)
        return defer.succeed(None)

    def pauseProducing(self):
        pass

    def stopProducing(self):
        pass


class BaseProtocol(Protocol):
    CLIENT = 'client'
    PROXY_CLIENT = 'proxy_client'
    HTTP_REQUEST = 'http_request'

    client_types = [
        CLIENT,
        PROXY_CLIENT,
        HTTP_REQUEST,
    ]

    def __init__(self, clients, run_module=None, http_requests=()):
        self.clients = clients
        self.client_type = None
        self.run_module = run_module
        self.http_requests = http_requests
        self.id = None

    def connectionLost(self, reason=connectionDone):
        if self.id in self.clients:
            del self.clients[self.id]

    def dataReceived(self, data):
        try:
            data = data.decode('utf-8') if data else ''
        except UnicodeDecodeError:
            return self.transport.loseConnection()

        if data == '\quit':
            return self.transport.loseConnection()

        # Определям тип клиента
        if not self.client_type:
            client_type = data.split('type:')[-1].strip()
            if client_type not in self.client_types:
                if 'HTTP/1.1' in data:
                    data = data.split('\r\n\r\n')[-1]
                    client_type = self.HTTP_REQUEST
                else:
                    self.transport.write(
                        'Bad client type: {}\r\nEnter valid type: "type:client"\r\n'
                        .format(client_type).encode('utf-8'))
                    return

            self.client_type = client_type
            self.id = str(uuid.uuid4())
            self.clients[self.id] = self

            if self.client_type != self.HTTP_REQUEST:
                return

        # Запуск скрипта
        if self.run_module:
            data = self.run_module.run(data)

        data = data.encode('utf-8')

        # Отправить всем исключая себя и HTTP запрос
        receivers = [
            client
            for client_id, client in self.clients.items()
            if client != self and client.client_type != self.HTTP_REQUEST
        ]

        for client in receivers:
            client.transport.write(data)

        # Отправить HTTP запросы
        if self.client_type != self.HTTP_REQUEST and self.http_requests:
            agent = Agent(reactor)
            for http_request in self.http_requests:
                d = agent.request(
                    method=b'GET',
                    uri='http://{host}:{port}'.format(**http_request).encode('utf-8'),
                    bodyProducer=StringProducer(data),
                )

        # Если HTTP клиент - вернуть 200 статус
        if self.client_type == self.HTTP_REQUEST:
            self.transport.write(b'HTTP/1.1 200 OK')
            return self.transport.loseConnection()


class ServerFactory(Factory):
    def __init__(self, run_module=None, http_requests=()):
        self.clients = {}
        self.run_module = run_module
        self.http_requests = http_requests

    def buildProtocol(self, addr):
        return BaseProtocol(self.clients, self.run_module, self.http_requests)
