def run(message):
    print("Run file {file}. Send message:\n {line}.".format(
        file=__file__,
        line=message
    ))
    return message
