import os
import jinja2
import asyncio
import aiohttp_jinja2

from aiohttp import WSMsgType
from aiohttp.web import Application, run_app
from aiohttp.web_ws import WebSocketResponse


class Settings:
    pass


settings = Settings()

settings.BASE_DIR = os.path.dirname(__file__)
settings.TEMPLATE_DIR = os.path.join(settings.BASE_DIR, 'templates')


@aiohttp_jinja2.template('main.html')
async def main(request):
    return {}


async def ws_view(request):
    print('1111111111111')
    ws = WebSocketResponse()
    await ws.prepare(request)

    sockets = request.app['websockets']
    sockets.append(ws)

    async for msg in ws:
        if msg.tp == WSMsgType.TEXT:
            print(msg)
            if msg == 'quite':
                break

        elif msg.tp == WSMsgType.ERROR:
            pass

    sockets and sockets.remove(ws)
    return ws


class AsteriskProtocol(asyncio.Protocol):
    def __init__(self, app):
        self.app = app

    # def connection_made(self, transport):
    #     transport.write(self.message.encode())
    #     print('Data sent: {!r}'.format(self.message))

    def data_received(self, data):
        data = data.decode()
        print('Data received: {!r}'.format(data))
        sockets = self.app['websockets']

        for _ws in sockets:
            _ws.send_str(data)


async def get_app(current_loop):
    new_app = Application(
        loop=current_loop,
    )

    def asterisk_factory():
        return AsteriskProtocol(new_app)

    new_app['websockets'] = []

    new_app.router.add_get('/', main)
    new_app.router.add_get('/ws/', ws_view)

    aiohttp_jinja2.setup(new_app, loader=jinja2.FileSystemLoader(settings.TEMPLATE_DIR))

    new_app.asterisk_connection = await current_loop.create_connection(
        asterisk_factory, '127.0.0.1', 10000
    )
    return new_app

loop = asyncio.get_event_loop()
app = loop.run_until_complete(get_app(loop))
run_app(app)
