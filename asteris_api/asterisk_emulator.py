import json

from twisted.internet import reactor, task
from twisted.internet.protocol import Factory, Protocol


class AsteriskProtocol(Protocol):
    def connectionMade(self):
        task.LoopingCall(self.send_data).start(5)

    def dataReceived(self, data):
        if data:
            self.transport.loseConnection()

    def send_data(self):
        self.transport.write(
            json.dumps({'key1': 'value1', 'key2': 'value2'}).encode('utf-8')
        )
        self.transport.write(b'\r\n\r\n')


class AsteriskFactory(Factory):
    def __init__(self):
        self.clients = {}

    def buildProtocol(self, addr):
        return AsteriskProtocol()


reactor.listenTCP(10000, AsteriskFactory())
reactor.run()
